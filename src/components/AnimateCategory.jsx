import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { Link, useLocation } from 'react-router-dom'

export default function AnimateCategory() {

    const location = useLocation()
    const query = new URLSearchParams(location.search)
    let name = query.get("type")
    return (
        <>
            <h2 style={{ fontWeight: "bold" }}>Animation Category</h2>
            <ButtonGroup aria-label="Basic example">
                <Link to="/video/animation/?type=Action"><Button variant="secondary">Action</Button></Link>
                <Link to="/video/animation/?type=Romance"><Button variant="secondary">Romance</Button></Link>
                <Link to="/video/animation/?type=Comedy"><Button variant="secondary">Comedy</Button></Link>
            </ButtonGroup>
            <h4>Please Choose Category: <span style={{ color: "red" }}>{name}</span></h4>
        </>
    )
}
