import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card, Button, Col,Row, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom';


export default function MyCard(props) {

  console.log(props.movies);


  return (
    <Container>
    <Row className="p-2">
      {props.movies.map((item, index) => (
        <Col className="py-2" sm={3} key={index}> 
          <Card style={{ width: '16rem' }}>
            <Card.Img variant="top" src={item.thumbnail} />
            <Card.Body>
              <Card.Title>{item.title}</Card.Title>
              <Card.Text>
                {item.content}
              </Card.Text>
             <Link to={`/detail/${item.id}`}> <Button variant="info"> Read </Button></Link>
            </Card.Body>
          </Card>
        </Col>
     
      ))
      }
       </Row>
    </Container>
  )
}
