import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar ,Nav,Button, Form, FormControl} from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function MyMenu() {
    return (
        <Navbar bg="light" variant="light">
        <Navbar.Brand href="#home">React-Router</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/video">Video</Nav.Link>
          <Nav.Link as={Link} to="/accout">Account</Nav.Link>
          <Nav.Link as={Link} to="/welcome">Welcome</Nav.Link>
          <Nav.Link as={Link} to="/auth">Auth</Nav.Link>
        </Nav>
        <Form inline>
          <FormControl type="text" placeholder="Search" className="mr-sm-2" />
          <Button variant="outline-primary">Search</Button>
        </Form>
      </Navbar>
    )
}
