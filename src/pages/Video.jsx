import React, { Component } from 'react'
import { Container, Button, ButtonGroup } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import AnimateCategory from '../components/AnimateCategory';
import MovieCategory from '../components/MovieCategory';


export default class Video extends Component {
    render() {
        return (
            <Router>
                <Container>
                    <h1 style={{ fontWeight: "bold" }}>Video</h1>
                    <ButtonGroup aria-label="Basic example">
                        <Link to="/video/movie"> <Button variant="secondary">Movie</Button></Link>
                        <Link to="/video/animation"> <Button variant="secondary">Animation</Button></Link>
                    </ButtonGroup>
                    <Switch>
                        <Route exact path="/video/movie" component={MovieCategory}/>
                        <Route exact path="/video/animation" component={AnimateCategory}/>
                    </Switch>
                </Container>
                </Router>
        )
    }
}
