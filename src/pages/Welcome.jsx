import React from 'react'
import { Container, Button } from 'react-bootstrap'

export default function Welcome(props) {
    return (
        <Container>
            <h1>Welcome</h1>
            <Button onClick={props.signOut} variant="primary">Logout</Button>
        </Container>
    )
}
