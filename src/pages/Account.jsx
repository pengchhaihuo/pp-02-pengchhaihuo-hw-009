
import { Container } from 'react-bootstrap'
import React from 'react'
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom'
import DisplayAccoutI from '../components/DisplayAccoutI'
export default function Account() {
    return (
        <Router>
            <Container className="p-2">
                <h2>Account</h2>
                <ul>
                    <li><Link to={`/account/netflix`} >Netflix</Link></li>
                    <li><Link to={`/account/Zillow`} >Zillow</Link></li>
                    <li><Link to={`/account/Yahoo`} >Yahoo</Link></li>
                    <li><Link to={`/account/Modus-Create`} >Modus Crete</Link></li>
                </ul>
            <Switch>
                <Route path="/account/:id" component={DisplayAccoutI}/>
            </Switch>
            </Container>
        </Router>
    )
}

