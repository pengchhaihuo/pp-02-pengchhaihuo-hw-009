
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button } from 'react-bootstrap'


const mystyle = {
    paddingTop: "20px",
    width: "40%",
    margin: "auto"
  };
export default function Auth(props) {

    


    return (
        <div style={mystyle}>
            <Form>
                <h2 className="pb-1">Login</h2>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <Button onClick={props.signIn} variant="primary" >
                    Login
            </Button>
            </Form>
        </div>
    )
}
