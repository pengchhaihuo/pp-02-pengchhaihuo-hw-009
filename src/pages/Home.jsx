import React from 'react'
import MyCard from '../components/MyCard'
import  { Component } from 'react'


export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
        movies: [
            {
                id: 1,
                title: "Seven Deadly Sins",
                content: "Content",
                thumbnail: "/images/Seven-Deadly-Sins.jpg"
            },
            {
                id: 2,
                title: "Demon Slayer",
                content: "Content",
                thumbnail: "/images/Demon-slayer.jpg"
            },
            {
                id: 3,
                title: "Sword Art Online",
                content: "Content",
                thumbnail: "/images/Sword-art-online.jpg"
            },
            {
                id: 4,
                title: "Naruto",
                content: "Content",
                thumbnail: "/images/Naruto.jpg"
            },
            {
                id: 5,
                title: "Jujutsu Kaisen",
                content: "Content",
                thumbnail: "/images/jujutsu-kaisen-15.jpg"
            },
            {
                id: 6,
                title: "One Punch",
                content: "Content",
                thumbnail: "/images/one-punch-man.jpg"
            }
        ]
    }
}

  render() {
    return (
      <MyCard movies={this.state.movies}/>
    )
  }
}
