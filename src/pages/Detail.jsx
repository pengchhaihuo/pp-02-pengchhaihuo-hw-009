import React from 'react'
import {Container} from 'react-bootstrap';
import { useParams } from 'react-router';
export default function Detail() {
  

    let param = useParams()
    return (
        <Container>
            {/* <p>Detail: {match.params.id}</p> */}
            <h3>Detail : {param.id}</h3>
        </Container>
            
      
    )
}
