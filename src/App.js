import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import MyMenu from './components/MyMenu';
import Home from './pages/Home';
import Video from './pages/Video';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Auth from './pages/Auth';
import Account from './pages/Account';
import Welcome from './pages/Welcome';

import Detail from './pages/Detail';
import ProtectRoute from './pages/ProtectRoute';



export default class App extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            isSignin: false
        }
     
    }
    signIn = ()=>{  
        this.setState({
            isSignin: true
        })
        console.log(this.state.isSignin);
    }
    signOut = ()=>{  
        
        this.setState({
            isSignin: false
        })
         console.log(this.state.isSignin);
    }
    


    render() {
        return (
            <Router>
                <MyMenu />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/video" component={Video} />
                    <Route path="/accout" component={Account} />
                    <ProtectRoute isSignin={this.state.isSignin} component={Welcome} path="/welcome"/>
                    <Route path="/auth" render={()=><Auth  signIn={this.signIn} />}/>
               <Route path="/detail/:id" component={Detail} />
               </Switch>
            </Router>
        )
    }
}
